How to install Entity Demo Module
---------------------------------

1) Go to mywebsite.test/admin/modules
2) Enable Entity Demo and Entity demo UI
3) Clear all drupal caches (mywebsite.test/admin/config/development/performance)


How to manage Entity Demo type (Manage fields & Manage display)
---------------------------------------------------------------

1) Go to mywebsite.test/admin/structure/entity_demo_type/fields
2) Go to mywebsite.test/admin/structure/entity_demo_type/display


How to view, create, edit or delete Entity Demo Instance
--------------------------------------------------------

1) Entity Demo list : Go to mywebsite.test/admin/content/entitydemo-list
2) Create an entity demo : click on "Create an entity demo" link
   mywebsite.test/admin/content/entitydemo-list/add
3) To edit or delete an entity demo click on "edit" or "delete" links


To be aware
-----------

1) entitydemo-list is a view. You can add or remove columns easily. Go to mywebsite.test/admin/structure/views (Entity demo list)
2) You can add fields easily : mywebsite.test/admin/structure/entity_demo_type/fields
   "New field demo" is a field which exported with features module and attached in code (entitydemo.install)
3) You can manage display fields easily : mywebsite.test/admin/structure/entity_demo_type/display

